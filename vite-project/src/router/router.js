import { createRouter,createWebHistory} from "vue-router";

// 路由信息
const routes = [
    {
        path: "/",
        name: "Index",
        component:  () => import('../pages/index.vue'),
    },
    {
        path: "/page2",
        name: "Page2",
        component:  () => import('../pages/page2.vue'),
    },
    {
        path: "/users/:username/posts/:postId",
        name: "Users",
        component:  () => import('../pages/Users.vue'),
    },

    
];

// 导出路由
const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;