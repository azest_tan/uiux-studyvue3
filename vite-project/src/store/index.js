import { createStore } from 'vuex'
import UserInfo from '../store/modules/userinfo'

export default createStore({
    state: {
        count: 2000,
    },
    mutations: {
        setcount(state,step){
            state.count = step;
        }
    },
    actions: {
    },
    modules: {
        UserInfo:UserInfo,
    }
})
